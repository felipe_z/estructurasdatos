package mundo.facade;

import mundo.contenedora.Nodo;
import mundo.contenedora.Tabla;
import mundo.dao.SongDAO;
import mundo.db.Conexion;
import mundo.contenedora.Mensaje;

public class Facade implements IFacade
{
	private SongDAO SongsDao;
	
	public Facade()
	{
		SongsDao = new SongDAO();
	}

	@Override
	public void insert(Conexion con, Mensaje message) 
	{
		if(message.getTabla().equals(Tabla.SONGS))
		{
			SongsDao.insert(con, message);
		}
	}

	@Override
	public void delete(Conexion con, Mensaje message) 
	{
		if(message.getTabla().equals(Tabla.SONGS))
		{
			SongsDao.delete(con, message);
		}
	}
	
	
	@Override
	public void update(Conexion con, Mensaje message) 
	{	
		if(message.getTabla().equals(Tabla.SONGS))
		{
			SongsDao.update(con, message);
		}
	}

	
	@Override
	public Nodo consultAll(Conexion con, Mensaje message) {

		Nodo nvo = new Nodo<>();
		{
			if(message.getTabla().equals(Tabla.SONGS))
			{
				nvo = SongsDao.consultAll(con, message);
			}
		}
		return nvo;
	}

	
	@Override
	public Nodo consult(Conexion con, Mensaje message) 
	{
		Nodo nvo = new Nodo<>();
		{
			if(message.getTabla().equals(Tabla.SONGS))
			{
				nvo = SongsDao.consult(con, message);
			}
		}
		return nvo;
	}

}

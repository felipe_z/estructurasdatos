package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import mundo.contenedora.Tabla;


public class MenuInterfazSoundLife extends JMenuBar implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 46378926473L;

	public final static String SONG = "Song";
	
	public final static String EXIT = "Exit"; 
	
    /**
     * El menu Archivo.
     */
    private JMenu menuFile;
    
    /**
     * La opcion para abrir el menu cancion.
     */
    private JMenuItem itemSong;
    
    private InterfazSoundLife main;
    
    private JMenuItem itemExit;
    
    private Tabla tabla = null;
    
    
    public MenuInterfazSoundLife(InterfazSoundLife ia)
    {
    	main = ia;
    	
    	menuFile = new JMenu( "File" );
        menuFile.setMnemonic( KeyEvent.VK_A );
        add( menuFile );
        
        itemSong = new JMenuItem("To consult for song");
        itemSong.setActionCommand(SONG);
        itemSong.setAccelerator( KeyStroke.getKeyStroke( KeyEvent.VK_A, ActionEvent.CTRL_MASK ) );
        itemSong.setMnemonic( KeyEvent.VK_A );
        itemSong.addActionListener( (ActionListener) this );
        menuFile.add(itemSong);
        
        menuFile.addSeparator();
        
        itemExit = new JMenuItem( "Exit" );
        itemExit.setActionCommand( EXIT );
        itemExit.addActionListener( this );
        menuFile.add( itemExit );
        
    }

	@Override
	public void actionPerformed(ActionEvent e) 
	{
	
		String command = e.getActionCommand( );
		if(SONG.equals(command))
		{
			tabla = Tabla.SONGS;
			DialogueSong song = new DialogueSong();
			song.setVisible(true);
		}
		if(EXIT.equals(command))
		{
			main.dispose();
		}
		
	}
    
}
